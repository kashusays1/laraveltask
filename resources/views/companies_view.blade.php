<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>jQuery Shield UI Demos</title>
    <link id="themecss" rel="stylesheet" type="text/css" href="//www.shieldui.com/shared/components/latest/css/light/all.min.css" />
    <script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
</head>
			
		
<body class="theme-light">
<div class="container">

<form method="get" action="{{url('search')}}">
  <label for="fname">Company:</label><br>
  <input type="text" id="company" name="company" placeholder="Company Name"><br>
  <label for="lname">Department:</label><br>
  <input type="text" id="dept" name="dept" placeholder="Department"><br><br>
  <input type="submit" value="Submit">
</form> 
	    
@php
	  $prev_company = '';
	  $str = '';
@endphp

      @for($i = 0;  $i < count($companies) ; $i++)
		  
		  @if($prev_company != $companies[$i]->comp)
			@php
			  $prev_dept = '';
			@endphp
			  	  <ul>
							<li>{{$companies[$i]->comp}}
		   @endif	
		  
		  @if($prev_dept != $companies[$i]->dept) 
			<ul>
				<li>{{ $companies[$i]->dept }}
							
		  @endif
				<ul><li> {{ $companies[$i]->emp }} </li></ul>
		  
		  @if((!empty($companies[$i+1]->dept) && $companies[$i]->dept != $companies[$i+1]->dept) || empty($companies[$i+1]->dept))
			 </li></ul>			  
		  @endif
		  @if((!empty($companies[$i+1]->comp) && $companies[$i]->comp != $companies[$i+1]->comp)  || empty($companies[$i+1]->comp))
			 </li></ul>
		  @endif
	@php  
	  $prev_company = $companies[$i]->comp;
	  $prev_dept = $companies[$i]->dept;
    @endphp
	 

	 
@endfor


   
</div>
<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview").shieldTreeView();
    });
</script>
<style>
    .container
    {
        max-width: 400px;
        margin: auto;
    }
    .treeview-icon
    {
        width: 16px;
        height: 16px;
        background-image: url("/Content/img/file/file-icons-sprite.png");
    }
    .icon-folder
    {
        background-position: 0px 0px;
    }
    .icon-png
    {
        background-position: -16px 0px;
    }
    .icon-txt
    {
        background-position: -32px 0px;
    }
    .icon-pdf
    {
        background-position: -48px 0px;
    }
    .icon-doc
    {
        background-position: -64px 0px;
    }
    .icon-xls
    {
        background-position: -80px 0px;
    }
</style>
</body>
</html>