<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CompanyController extends Controller
{
    function index(){
		
	  $companies = DB::select('SELECT c.name comp, e.name emp, d.name dept from companies e INNER JOIN companies d ON e.relation = d.id INNER JOIN companies c ON c.id = d.relation order by comp, dept');
      return view('companies_view',['companies'=>$companies]);
	}
	function search(Request $request){
		$company = $request->company;
		$dept = $request->dept;
		$where = "WHERE 1 = 1";
		if(!empty ($company))
			$where .= " AND c.name like '%".$company."%'";
		
		if(!empty ($dept))
			$where .= " AND d.name like '%".$dept."%'";
		
		
		$companies = DB::select("SELECT c.name comp, e.name emp, d.name dept from companies e INNER JOIN companies d ON e.relation = d.id INNER JOIN companies c ON c.id = d.relation ".$where."  order by comp, dept");	
        return view('companies_view',['companies'=>$companies]);
		
	    		
	}
}
