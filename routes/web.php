<?php

use Illuminate\Support\Facades\Route;
//use App\Models\Employee;
//use App\Http\Controllers\CompanyController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('company', 'App\Http\Controllers\CompanyController@index');
Route::get('search', 'App\Http\Controllers\CompanyController@search');


Route::get('/', function () {
    return view('welcome');
});